"""
File : messagehandler.py
Author : Amin - CDL
Version 0.2
"""

import discord
from oniseprequest import OnisepRequest
from oniseprequest import OnisepException

CHANNELS_ID = [787096629026488380, 788706573282836510, 789532058766671922]


class MessageHandler(discord.Client):

    def __init__(self):
        super().__init__()
        self._onisep = OnisepRequest()

    @property
    def _help_msg(self) -> str:
        return "_ _\n" \
               "_ _\n" \
               "**OniSearch** est le bot de l'association **CDL** qui te permet de rechercher une formation sur" \
               " la base de l'Onisep (<https://www.onisep.fr>) !\n" \
               "Voici la liste des **commandes** qui te permettent d'utiliser le bot !\n" \
               "_ _\n" \
               "**!h** - Affiche les commandes disponibles.\n" \
               "**!s** - Recherche une fomation, exemple : **!s comptabilité**.\n" \
               "**!n** - Affiche la page de résultats suivante.\n" \
               "**!p** - Affiche la page de résultats précédente.\n" \
               "_ _\n" \
               "Si tu ne sais pas quelle(s) formation(s) tu souhaites chercher, commence par consulter ce lien :\n" \
               "<https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac>\n" \
               "_ _\n" \

    @staticmethod
    def _create_embed_msg_from_results(results: list):
        embed_msg = discord.Embed(title="Résultat(s)", description="", colour=discord.Colour.greyple(),
                                  type='rich')
        for entry in results:
            str_value = str(entry[0]) + " - " + str(entry[2]) if str(entry[2]) != "" else str(entry[0]) + " "
            embed_msg.add_field(name="Nom de la formation",
                                value="[" + str_value + "]" + "(" + str(entry[1]) + ")")

        for i in range(5):
            embed_msg.add_field(name="\u200B", value="\u200B")
        embed_msg.add_field(name="Navigation\n_ _", value="Précédent: **!p** - Suivant: **!n**\n")

        return embed_msg

    @staticmethod
    async def _answer_no_result(message: discord.Message):
        response: discord.Message = await message.channel.send("Pas de résultat trouvé")
        await response.add_reaction("😕")

    @staticmethod
    async def _answer_error(message: discord.Message):
        response: discord.Message =  await message.channel.send("La base de l'Onisep semble rencontrer une erreur, "
                                                                "réessaie plus tard.")
        await response.add_reaction("😕")

    async def on_ready(self):
        pass

    async def on_message(self, message: discord.Message):
        if message.channel.id in CHANNELS_ID or type(message.channel) == discord.channel.DMChannel:
            try:

                if message.content.startswith("!h"):
                    await message.add_reaction('👍')
                    await message.channel.send(self._help_msg)

                elif message.content.startswith("!s"):
                    search_text = " ".join(message.content.split(' ')[1:])
                    if search_text == "":
                        await message.add_reaction('🤔')
                        await message.channel.send(self._help_msg)
                        return
                    await message.add_reaction('👍')
                    results = self._onisep.search(search_text)

                    if len(results) > 0:
                        msg = MessageHandler._create_embed_msg_from_results(results)
                        await message.channel.send(embed=msg)
                    else:
                        await MessageHandler._answer_no_result(message)

                elif message.content.startswith("!p"):
                    await message.add_reaction('👍')
                    results = self._onisep.previous()
                    if len(results) > 0:
                        msg = MessageHandler._create_embed_msg_from_results(results)
                        await message.channel.send(embed=msg)
                    else:
                        await MessageHandler._answer_no_result(message)

                elif message.content.startswith("!n"):
                    await message.add_reaction('👍')
                    results = self._onisep.next()
                    if len(results) > 0:
                        msg = MessageHandler._create_embed_msg_from_results(results)
                        await message.channel.send(embed=msg)
                    else:
                        await MessageHandler._answer_no_result(message)

                # elif str(message.author) != "OniSearch#2370":
                #     await message.add_reaction('🤔')
                #     await message.channel.send(self._help_msg)

                elif message.content.startswith("!"):
                    await message.add_reaction('🤔')
                    await message.channel.send(self._help_msg)

            except OnisepException as e:
                print(e)
                await MessageHandler._answer_error(message)
